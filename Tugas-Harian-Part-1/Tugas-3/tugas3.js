// soal 1

var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

str_concat = kataPertama + " " + kataKedua[0].toUpperCase()+kataKedua.slice(1) + " " + kataKetiga.substring(0, kataKetiga.length - 1) + kataKetiga.slice(-1).toUpperCase() + " " + kataKeempat.toUpperCase();
console.log(str_concat);

// soal 2

var panjangPersegiPanjang = "8";
var lebarPersegiPanjang = "5";

var alasSegitiga= "6";
var tinggiSegitiga = "7";

var kelilingPersegiPanjang = (2* (parseInt(panjangPersegiPanjang) + parseInt(lebarPersegiPanjang)));
var luasSegitiga = (parseInt(alasSegitiga) * parseInt(tinggiSegitiga) / 2);

console.log(`Keliling Pesegi Panjang: ${kelilingPersegiPanjang}`);
console.log(`Luas segitiga : ${luasSegitiga}`);

// soal 3

var sentences= 'wah javascript itu keren sekali'; 

var firstWord= sentences.substring(0, 3); 
var secondWord = sentences.substring(4,14);
var thirdWord = sentences.substring(15,18);
var fourthWord = sentences.substring(19,24);  
var fifthWord= sentences.slice(25);

console.log('Kata Pertama: ' + firstWord); 
console.log('Kata Kedua: ' + secondWord); 
console.log('Kata Ketiga: ' + thirdWord); 
console.log('Kata Keempat: ' + fourthWord); 
console.log('Kata Kelima: ' + fifthWord);

// soal 4
var nilaiJohn = 80;
var nilaiDoe = 50;

var nilaiJohnConv;
var nilaiDoeConv;
// nilai john
if (nilaiJohn >= 80) {
    nilaiJohnConv = 'A';
} else if (nilaiJohn < 80 && nilaiJohn >= 70) {
    nilaiJohnConv = 'B';
} else if (nilaiJohn < 70 && nilaiJohn >= 60) {
    nilaiJohnConv = 'C';
} else if (nilaiJohn < 60 && nilaiJohn >= 50) {
    nilaiJohnConv = 'D';
} else if (nilaiJohn < 50) {
    nilaiJohnConv = 'E';
}
// nilai doe 
if (nilaiDoe >= 80) {
    nilaiDoeConv = 'A';
} else if (nilaiDoe < 80 && nilaiDoe >= 70) {
    nilaiDoeConv = 'B';
} else if (nilaiDoe < 70 && nilaiDoe >= 60) {
    nilaiDoeConv = 'C';
} else if (nilaiDoe < 60 && nilaiDoe >= 50) {
    nilaiDoeConv = 'D';
} else if (nilaiDoe < 50) {
    nilaiDoeConv = 'E';
}
console.log('NilaiJohn : ' + nilaiJohnConv);
console.log('NilaiDoe : ' + nilaiDoeConv);


// soal 5 
var tanggal = 1;
var bulan = 7;
var tahun = 1998;

switch (bulan) {
    case 1 : {
      console.log(`${tanggal} Januari ${tahun}`);
      break;
    }
    case 2 : {
      console.log(`${tanggal} Februari ${tahun}`);
      break;
    }
    case 3 : {
      console.log(`${tanggal} Maret ${tahun}`);
      break;
    }
    case 4 : {
      console.log(`${tanggal} April ${tahun}`);
      break;
    }
    case 5 : {
      console.log(`${tanggal} Mei ${tahun}`);
      break;
    }
    case 6 : {
      console.log(`${tanggal} Juni ${tahun}`);
      break;
    }
    case 7 : {
      console.log(`${tanggal} Juli ${tahun}`);
      break;
    }
    case 8 : {
      console.log(`${tanggal} Agustus ${tahun}`);
      break;
    }
    case 9 : {
      console.log(`${tanggal} September ${tahun}`);
      break;
    }
    case 10 : {
      console.log(`${tanggal} Oktober ${tahun}`);
      break;
    }
    case 11 : {
      console.log(`${tanggal} November ${tahun}`);
      break;
    }
    case 12 : {
      console.log(`${tanggal} Desember ${tahun}`);
      break;
    }
}