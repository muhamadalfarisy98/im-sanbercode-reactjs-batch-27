// LOOPING 

// Soal 1 

console.log('LOOPING PERTAMA');
let inc = 2;
while (inc <= 20) {
    console.log(`${inc} - I love coding`);
    inc += 2;
}
console.log('LOOPING KEDUA');
let dec = 20;
while (dec >= 2) {
    console.log(`${dec} - I will become a frontend developer`);
    dec -= 2
}

// soal 2
for (var inc = 1 ; inc <= 20; inc ++){
    if (inc % 2 == 0){
      console.log(`${inc} - Berkualitas`);
    }
    else if (inc % 2 != 0){
      if (inc % 3 == 0){
        console.log(`${inc} - I Love Coding`);
      }
      else{
        console.log(`${inc} - Santai`);
      }
    }
}

// soal 3
for (let line = "#"; line.length < 7; line += "#"){
    console.log(line);
}

// ARRAY

// soal 4 
var kalimat=["aku", "saya", "sangat", "sangat", "senang", "belajar", "javascript"]
kalimat.shift();
var kalimat_new = kalimat.slice(2);
kalimat_new.unshift(kalimat[0]);
console.log(kalimat_new.join(" "));

// soal 5 
var sayuran = [];
sayuran.push('Kangkung','Bayam','Buncis','Kubis','Timun','Seledri','Tauge');
sayuran.sort();
for (let i= 0 ; i < sayuran.length; i++){
    console.log(`${i+1}. ${sayuran[i]}`);
}