// soal 1 

/*
    Tulis code function di sini
*/
function luasPersegiPanjang(panjang, lebar ) {
    return panjang * lebar;
}
function kelilingPersegiPanjang(panjang, lebar) {
    return 2 * (panjang + lebar);
}
function volumeBalok(panjang, lebar, tinggi) {
    return panjang * lebar * tinggi ;
}
 
var panjang= 12;
var lebar= 4;
var tinggi = 8;
 
var luasPersegiPanjangValue = luasPersegiPanjang(panjang, lebar);
var kelilingPersegiPanjangValue = kelilingPersegiPanjang(panjang, lebar);
var volumeBalokValue = volumeBalok(panjang, lebar, tinggi);
console.log('-----soal 1-----');
console.log(luasPersegiPanjangValue) ;
console.log(kelilingPersegiPanjangValue);
console.log(volumeBalokValue);


// soal 2
/* 
    Tulis kode function di sini
*/
function introduce(name, age, address, hobby){
    return 'Nama saya ' + name + ', umur saya ' + age + ' tahun, alamat saya di ' + address + ', dan saya punya hobby yaitu ' + hobby +'!';
}
 
var name = "John";
var age = 30;
var address = "Jalan belum jadi";
var hobby = "Gaming";
 
var perkenalan = introduce(name, age, address, hobby)
console.log('-----soal 2-----');
console.log(perkenalan); // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan belum jadi, dan saya punya hobby yait



// soal 3 

var arrayDaftarPeserta = ["John Doe", "laki-laki", "baca buku" , 1992]
var objDaftarPeserta = {
    "nama"  : arrayDaftarPeserta[0],
    "jenis kelamin" : arrayDaftarPeserta[1],
    "hobi" : arrayDaftarPeserta[2],
    "tahun lahir" : arrayDaftarPeserta[3]
};

console.log('-----soal 3-----');
console.log(objDaftarPeserta);


// soal 4 
data = [
    {
        "nama": "Nanas",
        "warna": "Kuning",
        "adaBijinya": "tidak",
        "harga": 9000 ,
    },
    {
        "nama": "Jeruk" ,
        "warna": "Oranye",
        "adaBijinya": "ada",
        "harga": 8000
    },
    {
        "nama": "Semangka",
        "warna": "Hijau & Merah",
        "adaBijinya": "ada",
        "harga": 10000,
    },
    {
        "nama": "Pisang",
        "warna": "Kuning",
        "adaBijinya": "tidak",
        "harga": 5000,
    }
]

console.log('-----soal 4-----');
tidakberbiji = [];
for (var item in data) {
    if (data[item]['adaBijinya'] == "tidak"){
        data[item]['adaBijinya'] = false;
        tidakberbiji.push(data[item]);
    }
}
console.log(tidakberbiji);

// soal 5
/* 
    Tulis kode function di sini 
*/
function tambahDataFilm(nama, durasi, genre, tahun) {
    var data = {}
    data = {
        "nama" : nama,
        "durasi" : durasi,
        "genre" : genre,
        "tahun" : tahun
    }
    return data;
}

var dataFilm = [];

dataFilm.push(tambahDataFilm("LOTR", "2 jam", "action", "1999"));
dataFilm.push(tambahDataFilm("avenger", "2 jam", "action", "2019"));
dataFilm.push(tambahDataFilm("spiderman", "2 jam", "action", "2004"));
dataFilm.push(tambahDataFilm("juon", "2 jam", "horror", "2004"));

console.log('-----soal 5-----');
console.log(dataFilm);