// arrow function
const myFunction = (x,y) => {
    return x*y;
  }
  
  console.log(myFunction(3,4));
  
  // enhanced object literals
  const fullName = 'John Doe';
  const john = {fullName};
  console.log(john);
  
  // desctructuring array
  let numbers = [1,2,3];
  
  const [numberOne,numberTwo,numberThree] = numbers
  console.log(numberOne);
  
  var studentName = {
    firstName : 'Faris',
    lastName : 'Kudo'
  };
  const {firstName,LastName} = studentName;
  console.log(firstName)
  
  
  //  Rest Paramaters
  let scores = [1,2,3,4,5,6];
  let [first,second,third, ...restOfparams] = scores;
  console.log(restOfparams);
  
  const filter = (start, ...rest) => {
    return rest.filter(el => el.text !==undefined)
  }
  
  console.log(filter(1,{text:"wonderful"}, "next"))
  
  // const fullName = (...rest) =>{ 
  //   let [firstName, lastName] = rest;
  // return firstName + lastName;
  // } 
  
  // console.log(fullName("John", "Doe")); // John Doe
  
  // spread operator
  
  let array1 = ['one', 'two'];
  let array2 = ['three', 'four'];
  let array3 = ['five', 'six'] ;  
  
  // // before es6
  // var combinedArray = array1.concat(array2).concat(array3)
  // console.log(combinedArray); // ['one', 'two', 'three', 'four', 'five', 'six']  
  
  // // after es6
  let combinedArray = [...array1, ...array2, ...array3]
  console.log(combinedArray); // ['one', 'two', 'three', 'four', 'five', 'six']
  
  
  //Spread in object
  // elemen dapat ditambahkan ke dalam object atau array baru
  let person = {name: "john", age: 30}
  
  let newPerson = {...person, hobby: "Gaming"}
  
  console.log(newPerson) // {name: "john", age: 30, hobby: "Gaming"}
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  