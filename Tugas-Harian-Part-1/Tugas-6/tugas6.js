// soal 1
const Pi = 22/7;
const luasLingkaran = (radius) => {
    return Pi * radius * radius
}
const kelilingLingkaran = (radius) => {
    return 2 * Pi * radius;
}
console.log('----soal 1----');
console.log(luasLingkaran(7));
console.log(kelilingLingkaran(7));

// soal 2
const introduce = (...rest) => {
  let [name, age, gender,job] = rest;
  if (gender == 'Laki-Laki') {
    gender = 'Pak';
  } else if (gender == 'Perempuan') {
    gender = 'Buk';
  }
  return `${gender} ${name} adalah seorang ${job} yang berusia ${age} tahun`;
}

console.log('\n----soal 2----');
//kode di bawah ini jangan dirubah atau dihapus
const perkenalan = introduce("John", "30", "Laki-Laki", "penulis");
console.log(perkenalan) ;// Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun"

// soal 3
const newFunction = (firstName, lastName) => {
  
  const name = {firstName, 
    lastName, 
    fullName: fullName = () => {
      console.log(`${firstName} ${lastName}`);
    }
  }
  return name;
}

console.log('\n----soal 3----');
// kode di bawah ini jangan diubah atau dihapus sama sekali
console.log(newFunction("John", "Doe").firstName)
console.log(newFunction("Richard", "Roe").lastName)
newFunction("William", "Imoh").fullName()


// soal 4
let phone = {
   name: "Galaxy Note 20",
   brand: "Samsung",
   year: 2020,
   colors: ["Mystic Bronze", "Mystic White", "Mystic Black"]
}
// kode diatas ini jangan di rubah atau di hapus sama sekali

/* Tulis kode jawabannya di sini */
var {name:phoneName ,brand: phoneBrand, year, colors} = phone;
var [colorBronze,colorWhite,colorBlack] = colors;
console.log('\n----soal 4----');
// kode di bawah ini jangan dirubah atau dihapus
console.log(phoneBrand, phoneName, year, colorBlack, colorBronze)

// soal 5

let warna = ["biru", "merah", "kuning" , "hijau"]

let dataBukuTambahan= {
  penulis: "john doe",
  tahunTerbit: 2020 
}

let buku = {
  nama: "pemograman dasar",
  jumlahHalaman: 172,
  warnaSampul:["hitam"]
}
// kode diatas ini jangan di rubah atau di hapus sama sekali

/* Tulis kode jawabannya di sini */ 
console.log('\n----soal 5----');
let buku_new = {...dataBukuTambahan, ...buku, warnaSampul: [...buku.warnaSampul, ...warna]};
console.log(buku_new);

















