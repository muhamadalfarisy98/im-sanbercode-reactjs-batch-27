// class
class Car {
	constructor(brand) {
		this.carname = brand;
	}
	present(){
		return 'i have a ' + this.carname;
	}
}

mycar = new Car("Ford");
console.log(mycar.present());

// static method
// cuma bisa dipanggil dari kelasnya langsung
class Car {
	constructor(brand) {
		this.username = brand;
	}
	static hello(){
		return "Hello!";
	}
}
// mycar = new Car("Ford");
console.log(Car.hello());

// inherited class
class Car {
	constructor(brand) {
		this.carname = brand;
	}
	present(){
		return 'i have a ' + this.carname;
	}
}

class Model extends Car {
	constructor(brand, mod) {
		super(brand);
		// mengacu ke class asalnya
		this._mod = mod;
	}
	show(){
		return this.present() + ' it is a ' + this._mod;
	}
}

mycar = new Model("Ford", "Mustang");
console.log(mycar.show());

