// soal 1

// release 0
class Animal {
    constructor(name, legs= 4, cold_blooded = false){ 
		this.name = name;
		this.legs = legs;
		this.cold_blooded = cold_blooded;
    }
    set legs(params) {
        this.kaki = params;
    }
	get legs(){
		return this.kaki;
	}


}
var sheep = new Animal("shaun");
console.log('----SOAL 1----');
console.log('----Release 0----');
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false
sheep.legs = 3
console.log(sheep.legs)

// release 1 
class Ape extends Animal {
	constructor(name, legs= 4, cold_blooded = false) {
		super(name,legs,cold_blooded);
	}
	yel() {
		console.log('Auooo');
	}
}

class Frog extends Animal {
	constructor(name, legs= 4, cold_blooded = false) {
		super(name,legs,cold_blooded);
	}
	jump() {
		console.log('hop hop');
	}
}

console.log('----Release 1----');
var sungokong = new Ape("kera sakti")
sungokong.yel(); // "Auooo"
sungokong.legs = 2
console.log(sungokong.name)
console.log(sungokong.legs)
console.log(sungokong.cold_blooded)

var kodok = new Frog("buduk")
kodok.jump() // "hop hop"
console.log(kodok.name)
console.log(kodok.legs)
console.log(kodok.cold_blooded)

// soal 2
class Clock {
    constructor({template}, timer){
		this.template = template;
        this.timer = timer;
	}
	render(){
		var date = new Date();

	    var hours = date.getHours();
	    if (hours < 10) hours = '0' + hours;
	
	    var mins = date.getMinutes();
	    if (mins < 10) mins = '0' + mins;
	
	    var secs = date.getSeconds();
	    if (secs < 10) secs = '0' + secs;
		
	    var output = this.template
	      .replace('h', hours)
	      .replace('m', mins)
	      .replace('s', secs);
	
	    console.log(output);
	}
	
	stop(){
		clearInterval(this.timer);
	}
	
	start(){
		this.render();
    	this.timer = setInterval(() => this.render(), 1000);
	}
	
}
console.log('\n----SOAL 2----');
var clock = new Clock({template: 'h:m:s'});
clock.start();  

