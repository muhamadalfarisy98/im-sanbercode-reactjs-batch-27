var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
const mulaiBaca = (remainingTime, books, idx) => {
    readBooksPromise (remainingTime, books[idx])
    .then((res) => {
        const nextBook = idx + 1;
        if (nextBook < books.length) {
            mulaiBaca(res, books, nextBook);
        }
    })
    .catch ((err) => {
        console.log(err.message);
    }
    )
}
mulaiBaca(10000, books, 0);