var filterBooksPromise = require('./promise2.js')
 
// Lanjutkan code untuk menjalankan function filterBookPromise
// promise tanpa async wait

// CARA 1

const warna_40 = (coolorful, page) => {
    filterBooksPromise (coolorful, page)
    .then((res) => {
        console.log(res);
    })
    .catch ((err) => {
        console.log(err.message);
    }
    )
}
warna_40(true, 40);

const tidakWarna_250 = async (color,page) => {
    try {
		const filteredBooks = await filterBooksPromise(color,page)
  		console.log(filteredBooks)
	} catch (err){
		console.log(err.message);
	}
}

tidakWarna_250(false, 250);

const warna_30 = async (color,page) => {
    try {
		const coloredBooks = await filterBooksPromise(color,page)
  		console.log(coloredBooks)
	} catch (error){
		console.log(error.message);
	}
}
warna_30(true, 30);

// CARA 2

// const execute = async () => {
//     try {
//         const val1 = filterBooksPromise(true, 40);
//         console.log(val1);
//     }
//     catch(err){
//         console.log(err.message);
//     }

//     try {
//         const val2 = await filterBooksPromise(false, 250);
//         console.log(val2);
//     }
//     catch(err){
//         console.log(err.message);
//     }

//     try {
//         const val3 = await filterBooksPromise(true, 30);
//         console.log(val3);
//     }
//     catch(err){
//         console.log(err.message);
//     }
// }

// execute();