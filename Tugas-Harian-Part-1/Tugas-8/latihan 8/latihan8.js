// callback 
function Displayer(some){
	console.log(some);
}

function myCal(num1, num2, myCallback){
	let sum = num1 + num2;
	myCallback(sum);
}

myCal(5,5, Displayer);

function periksaAntrianDokter(nomerAntri, callback) {
    console.log(`sekarang antrian ke-${nomerAntri}`)
    setTimeout(function () {
      if(nomerAntri === 10 ) { 
        console.log("saya masuk ruangan dokter")
        callback(0)
      } else {
        console.log("saya masih menunggu")
        callback(nomerAntri+1)
      }    
    }, 1000)
  }
  var nomorAntriSekarang = 7;
  
  periksaAntrianDokter(nomorAntriSekarang, function(nomorAntriBaru){
    periksaAntrianDoktera// callback

    function Displayer(some){
        console.log(some);
    }
    
    function myCal(num1, num2, myCallback){
        let sum = num1 + num2;
        myCallback(sum);
    }
    
    myCal(5,5, Displayer);
    
    // waiting for a Timeout
    // setTimeout(myCal(5,5, Displayer),3000);
    
    setTimeout(function() {
        myFunction("I love You !!!"); 
    }, 3000);
    
    console.log('ini dijalanin dulu gens');
    function myFunction(value) {
       console.log(value);}
    
    console.log('\n contoh 2 callback');
    function periksaDokter(nomerAntri, callback) {
        if(nomerAntri > 50 ) {
            callback(false)
        } else if(nomerAntri < 10) {
            callback(true)
        }    
    } 
    
    periksaDokter(52, function(check){
        if(check==true){
            console.log(check);
        }
        else {
            console.log('HIYAA');
        }
    })
    
    
    function periksaAntrianDokter(nomerAntri, callback) {
      console.log(`sekarang antrian ke-${nomerAntri}`)
      setTimeout(function () {
        if(nomerAntri === 10 ) { 
          console.log("saya masuk ruangan dokter")
          callback(0)
        } else {
          console.log("saya masih menunggu")
          callback(nomerAntri+1)
        }    
      }, 1000)
    }
    // callback
    // periksaAntrianDokter(5, function(params){
    // 	console.log(params);
    // })
    
    var nomorAntriSekarang = 7;
    
    periksaAntrianDokter(nomorAntriSekarang, function(nomorAntriBaru){
      periksaAntrianDokter(nomorAntriBaru, function(nomorAntriBaru1){
        periksaAntrianDokter(nomorAntriBaru1, function(nomorAntriBaru2){
          periksaAntrianDokter(nomorAntriBaru2, function(nomorAntriBaru3){
            return nomorAntriBaru3
          })
        })
      })
    });(nomorAntriBaru, function(nomorAntriBaru1){
      periksaAntrianDokter(nomorAntriBaru1, function(nomorAntriBaru2){
        periksaAntrianDokter(nomorAntriBaru2, function(nomorAntriBaru3){
          return nomorAntriBaru3
        })
      })
    })
  });


  // promise javascript
var isMomHappy = false;
 
// Promise
var willIGetNewPhone = new Promise(
    function (resolve, reject) {
        if (isMomHappy) {
            var phone = {
                brand: 'Samsung',
                color: 'black'
            };
            resolve(phone); // fulfilled atau janji dipenuhi
        } else {
            var reason = new Error('mom is not happy');
            reject(reason); // reject (ingkar)
        }
 
    }
); 

function askMom() {
    willIGetNewPhone
        .then(function (fulfilled) {
            // yay, you got a new phone
            console.log(fulfilled);
         // output: { brand: 'Samsung', color: 'black' }
        })
        .catch(function (error) {
            // oops, mom don't buy it
            console.log(error.message);
         // output: 'mom is not happy'
        });
}
 
// Tanya Mom untuk menagih janji
askMom() 


// contoh 2 promise
const periksaAntrian = (nomor) => {
	console.log('antrian ke ' + nomor);
	return new Promise ((resolve,reject) => {
		setTimeout(function () {
			if(nomor == 10){
				console.log('masuk gan');
				reject(0);
			}
			else {
				console.log('nunggu dulu gan');
				resolve(nomor + 1);
			}
		},1000)
	}
	)
}

// manggil promisenya
const execute = (antrian) => {
	periksaAntrian (antrian)
	.then((res) => {
		// console.log(res);
		execute(res);
	})
	.catch ((err) => {
		console.log(err);
	})
}
execute(7);


// async/wait
function doAsync() {
    return new Promise( function (resolve, reject){
      var check = true
      if (check){
        resolve("berhasil")
      }else{
        reject("gagal")
      }
    })
  }
  
   async function hello(){
     var result = await doAsync()
     console.log(result)
  }
  
  hello()
  
  
  // promise periksa data pasien
  function periksaDataPasien(nomorIdPasien) {
    var dataPasien = [
      {id: 1, nama: "John", jenisKelamin: "Laki-laki"},
      {id: 2, nama: "Michael", jenisKelamin: "Laki-laki"},
      {id: 3, nama: "Sarah", jenisKelamin: "Perempuan"},
      {id: 4, nama: "Frank", jenisKelamin: "Laki-laki"}
    ]
    return new Promise( function (resolve, reject){
      var pasien = dataPasien.find(x=> x.id === nomorIdPasien)
      if (pasien === undefined){
        reject("data pasien tidak ada")
      }else{
        resolve(pasien)
      }
    })
  }
  
  // without try catch error
  // const periksaPasien = async () => {
  // 	const dataJohn = await periksaDataPasien(4)
  //   	console.log(dataJohn)
  // }
  
  // with try catch
  const periksaPasien = async () => {
      try {
          const dataJohn = await periksaDataPasien(5)
            console.log(dataJohn)
      } catch (err){
          console.log(err);
      }
  }
  
  periksaPasien()
  
  // Serial dan paralel asyncronous
  
  const firstPromise = () =>{
    return new Promise ((resolve,reject) =>{
      setTimeout(()=>{
        resolve("first promise")
      },1000)
    })
  }
  
  const secondPromise = () =>{
    return new Promise ((resolve,reject) =>{
      setTimeout(()=>{
        resolve("second promise")
      },1000)
    })
  }
  
  const thirdPromise = () =>{
    return new Promise ((resolve,reject) =>{
      setTimeout(()=>{
        resolve("third promise")
      },1000)
    })
  }
  
  //ini paralel selama satu detik
  const asyncParalel = async () =>{
    firstPromise().then(res=>{
      console.log(res)
    })
    secondPromise().then(res=>{
      console.log(res)
    })
    thirdPromise().then(res=>{
      console.log(res)
    })
    
  }
  
  // ini berseri selama tiga detik
  const asyncSerial = async () =>{
    let a = await firstPromise()
    console.log(a)
    let b = await secondPromise()
    console.log(b)
    let c = await thirdPromise()
    console.log(c)
  }
  
  asyncParalel()
  asyncSerial()
  
  
  
  // why using async
  let angka = 1
  
  //sebelum menghandle asynchronous
  console.log("sebelum menghandle asynchronous")
  
  const beforeAsync = (angka) => {
      angka = 2
      console.log(angka)
  }
  
  beforeAsync(angka)
  console.log(angka)
  console.log()
  
  //setelah menghandle asynchronous
  console.log("setelah menghandle asynchronous")
  
  const myFunctionPromise = (angka) =>{
      return new Promise( (resolve, reject) => {
          resolve(angka = 2)
      })
  }
  
  const afterAsync = async (param) => {
      let output = await myFunctionPromise(param)
      console.log(output)
  }
  
  afterAsync(angka)
  console.log(angka)
  
