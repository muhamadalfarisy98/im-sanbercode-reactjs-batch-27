import React, { useEffect, useState } from "react"

const Tugas10 = () => {

    const [timer, setTimer] = useState(new Date())
    const [countdown, setCountdown] = useState(4)
    const [display, setDisplay] = useState(true)

    useEffect(() => {

        if (countdown !== 0) {
            const timer = setInterval(() => {
                setTimer(new Date())
                setCountdown(countdown - 1)
            }, 1000)

            return () => clearInterval(timer)
        }

        if(countdown === 0){
            setDisplay(false)
        }


    }, [countdown])

    return (
        <>
            {display ?
                <h1 className="container-tugas10">
                    <p>Now At : {timer.toLocaleTimeString()}</p>
                    <p className="countdown">Countdown : {countdown} </p>
                </h1> : false }
        </>
    )
}

export default Tugas10