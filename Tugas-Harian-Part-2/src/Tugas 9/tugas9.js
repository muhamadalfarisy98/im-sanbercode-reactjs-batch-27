import React from "react"
import Logo from "../assets/img/logo.png"

// props 
const ListComponent = (props) => {
    return (
        <div className="input-checkbox">
            <input type="checkbox"/><p>{props.name}</p>
        </div>
    )
}


const Tugas9 = () => {
    return (
    <div className="card">
        <img src= {Logo}/>
        <p>THINGS TO DO</p>
        <small>During Bootcamp in Sanbercode</small>
        <hr/>
        
        <ListComponent name = "Belajar Git &amp; CLI"/>
        <ListComponent name = "Belajar HTML &amp; CSS"/>
        <ListComponent name = "Belajar JavaScript"/>
        <ListComponent name = "Belajar ReactJS Dasar"/>
        <ListComponent name = "Belajar ReactJS Advance"/>

        <button>Send</button>

    </div>

    )

}


export default Tugas9;
